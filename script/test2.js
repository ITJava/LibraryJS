console.log("JS test log 2.0")
function Human(){
	this.age = 28;
}
//var someHuman = new Human();
function Girl(){
	Human.apply(this);
	//this.__proto__ = someHuman;
	this.name = "Kelly";
}
//Girl.prototype = someHuman;
var object = new Girl();

console.log(object.__proto__);
console.log(object.name);
console.log(object.age);